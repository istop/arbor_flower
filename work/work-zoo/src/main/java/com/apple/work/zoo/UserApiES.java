package com.apple.work.zoo;

import com.apple.api.UserApi;

public class UserApiES implements UserApi {

    @Override
    public String sayHello(String words) {
        System.out.println("这是ES 的实现");
        if (words != null) {
            return "ES -sayHello-hello-" + words;
        }
        return "ES ";
    }

}
