package com.apple.work.zoo;

import com.apple.api.UserApi;

public class UserApiMongo implements UserApi {


    @Override
    public String sayHello(String words) {
        System.out.println("这是MongoDB 的实现");
        if (words != null) {
            return "mongoDB-sayHello-hello-" + words;
        }
        return "mongoDB";
    }

}
