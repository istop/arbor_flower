package comapple.work.dot;

import com.apple.api.UserApi;

import java.util.ServiceLoader;

public class TestSPI {

    public static void main(String[] args) {
        ServiceLoader<UserApi> userApis = ServiceLoader.load(UserApi.class);

        // todo 说明加载多个实例
        int work = 0;
        for (UserApi userApi : userApis) {
            userApi.sayHello("hello"+work);
            work += 5;
        }
    }
}
